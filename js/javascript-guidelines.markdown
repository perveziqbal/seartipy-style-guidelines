* Do not use uppercase letters in file or directory names. Use _ instead like hello_world.js
* Always have .gitignore and set it up properly.
* Always have a package.json file. All npm dependencies should be part of this file.
* Use either bower and/or npm/browserify for client side dependencies.
* Use a build automation tool like grunt, broccoli, gulp.
* For all your source code you must either have a unit test or an integration test. You shouldn't commit any code without at least on of them.

* Use PascalCase for types/classes and camelCase for everything else.
* Use es6 modules or browserify for modules, do not create global idenitifiers.
* Use underscore and avoid loops. If you need one, create a function to encapsulate the loop and use it.
* Do not create functions with more than four parameters.
* Do not check for undefined or null or type directly, use underscore isXyz functions
* Prefect revealing module pattern to prototypes. Use prototypes when performance matters.

* Always prefer functional construction of objects. Try avoiding setting a property name directly like foo.bar = 20. Consider merge/extend instead.
* Do not use timeout or $timeout. Underscore functions like delay when essential
* Try avoiding creating classes. Instead think in terms of arrays, simple data objects and functions.
* Do not create functions which need `this`, unless passing it to library functions which use this.
