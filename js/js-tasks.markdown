* lint your source code. Use jshint for javascript and coffeelint for coffeescript.
* create .jshintrc or coffeelint.json file for enforcing style guidelines
* create a grunt task to check for style guidelines voilation

* create a task to convert coffeescript to single javascript file
* create a task to convert all jade files to html files
* create a task to convert all less files to css files
* Use bower to satisfy client side dependencies
* creata a bower task if possible to minify all bower dependencies to a single file.

* learn and use mocha and/or bdd-qunit for unit testing. Use karma as test runner.
* learn and use browserify for creating modules on the client side. You could also consider using es6-module-transpiler.
* create a grunt task to server static files
* Using grunt enable livereload
* create a grunt task to execute your tests
* for angular, learn protractor and use it for integration testing
* Understand git workflow. Learn commit/push/pull, branch, fetch/merge, squash, rebase
* Create a grunt CI task to run jshint/coffeelint and unit tests. Setup git hook for running it before commit.
